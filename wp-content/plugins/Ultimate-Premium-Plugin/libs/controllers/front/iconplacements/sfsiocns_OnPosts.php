<?php
/* add fb like add this to end of every post */
function sfsi_plus_social_buttons_below($content,$inline=true)
{
	global $post;
	$socialObj    = new sfsi_plus_SocialHelper();	
	$sfsi_section6=  unserialize(get_option('sfsi_premium_section6_options',false));
	$sfsi_section5=  unserialize(get_option('sfsi_premium_section5_options',false));
	$option2=  unserialize(get_option('sfsi_premium_section2_options',false));
		 
	//new options that are added on the third questions
	//so in this function we are replacing all the past options 
	//that were saved under option6 by new settings saved under option8 
	$sfsi_section8=  unserialize(get_option('sfsi_premium_section8_options',false));
	$sfsi_plus_show_item_onposts = $sfsi_section8['sfsi_plus_show_item_onposts'];
	//new options that are added on the third questions
	
	//checking for standard icons
	if(!isset($sfsi_section8['sfsi_plus_rectsub']))
	{
		$sfsi_section8['sfsi_plus_rectsub'] = 'no';
	}
	if(!isset($sfsi_section8['sfsi_plus_rectfb']))
	{
		$sfsi_section8['sfsi_plus_rectfb'] = 'yes';
	}
	if(!isset($sfsi_section8['sfsi_plus_rectshr']))
	{
		$sfsi_section8['sfsi_plus_rectshr'] = 'yes';
	}
	if(!isset($sfsi_section8['sfsi_plus_recttwtr']))
	{
		$sfsi_section8['sfsi_plus_recttwtr'] = 'no';
	}
	if(!isset($sfsi_section8['sfsi_plus_rectpinit']))
	{
		$sfsi_section8['sfsi_plus_rectpinit'] = 'no';
	}
	if(!isset($sfsi_section8['sfsi_plus_rectfbshare']))
	{
		$sfsi_section8['sfsi_plus_rectfbshare'] = 'no';
	}
	if(!isset($sfsi_section8['sfsi_plus_rectlinkedin']))
	{
		$sfsi_section8['sfsi_plus_rectlinkedin'] = 'no';
	}
	if(!isset($sfsi_section8['sfsi_plus_rectreddit']))
	{
		$sfsi_section8['sfsi_plus_rectreddit'] = 'no';
	}
	//checking for standard icons
		
	$permalink 	  = $socialObj->sfsi_get_custom_share_link($post->ID);
	$title 		  = get_the_title();
	$sfsiLikeWith ="45px;";
    
    /* check for counter display */
	if($sfsi_section8['sfsi_plus_icons_DisplayCounts']=="yes")
	{
		$show_count=1;
		$sfsiLikeWith="75px;";
	}   
	else
	{
		$show_count=0;
	} 
        
	$txt 	=(isset($sfsi_section8['sfsi_plus_textBefor_icons']))? $sfsi_section8['sfsi_plus_textBefor_icons'] : "Please follow and like us:" ;
	$float	= $sfsi_section8['sfsi_plus_icons_alignment'];
	if($float == "center")
	{
		$style_parent= 'text-align: center;';
		$style = 'float:none; display: inline-block;';
	}
	else
	{
		$style_parent= 'text-align:'.$float.';';
		$style = 'float:'.$float.';';
	}

	if(
		$sfsi_section8['sfsi_plus_rectsub'] 	== 'yes' || 
		$sfsi_section8['sfsi_plus_rectfb'] 		== 'yes' || 
		$sfsi_section8['sfsi_plus_rectshr'] 	== 'yes' || 
		$sfsi_section8['sfsi_plus_recttwtr'] 	== 'yes' || 
		$sfsi_section8['sfsi_plus_rectpinit'] 	== 'yes' || 
		$sfsi_section8['sfsi_plus_rectlinkedin']== 'yes' ||
		$sfsi_section8['sfsi_plus_rectreddit'] 	== 'yes' ||
		$sfsi_section8['sfsi_plus_rectfbshare'] == 'yes'
	)
	{
		$icons="<div class='sfsi_plus_Sicons ".$float."' style='".$style.$style_parent."'>
			<div style='display: ".($inline?"inline-block":'block').";margin-bottom: 0; margin-left: 0; margin-right: 8px; margin-top: 0; vertical-align: middle;width: auto;'>
				<span>".$txt."</span>
			</div>";
	}
	if($sfsi_section8['sfsi_plus_rectsub'] == 'yes')
	{
		if($show_count){$sfsiLikeWithsub = "93px";}else{$sfsiLikeWithsub = "64px";}
		if(!isset($sfsiLikeWithsub)){$sfsiLikeWithsub = $sfsiLikeWith;}

		$icons.="<div class='sf_subscrbe' style='display: inline-block;vertical-align: middle;width: auto;'>".sfsi_plus_Subscribelike($permalink,$show_count)."</div>";
	}
	if($sfsi_section8['sfsi_plus_rectfb'] == 'yes')
	{
		if($show_count){}else{$sfsiLikeWithfb = "48px";}
		if(!isset($sfsiLikeWithfb)){$sfsiLikeWithfb = $sfsiLikeWith;}
        
        if($sfsi_section5['sfsi_plus_Facebook_linking'] == "facebookcustomurl")
        {
        	$userDefineLink = ($sfsi_section5['sfsi_plus_facebook_linkingcustom_url']);
        	$icons.="<div class='sf_fb' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsi_plus_FBlike($userDefineLink,$show_count)."</div>";
        }
        else
        {
			$icons.="<div class='sf_fb' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsi_plus_FBlike($permalink,$show_count)."</div>";
		}
	}
	if($sfsi_section8['sfsi_plus_rectfbshare'] == 'yes')
	{
		if($show_count){}else{$sfsiLikeWithfb = "48px";}
		$permalink = $socialObj->sfsi_get_custom_share_link('facebook');        	
		$icons.="<div class='sf_fb' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsiFB_Share($permalink,$show_count)."</div>";
	}		
	if($sfsi_section8['sfsi_plus_recttwtr'] == 'yes')
	{
		if($show_count){$sfsiLikeWithtwtr = "77px";}else{$sfsiLikeWithtwtr = "56px";}
		if(!isset($sfsiLikeWithtwtr)){$sfsiLikeWithtwtr = $sfsiLikeWith;}
	
		$permalink = $socialObj->sfsi_get_custom_share_link('twitter');	
		$icons.="<div class='sf_twiter' style='display: inline-block;vertical-align: middle;width: auto;'>".sfsi_plus_twitterlike($permalink,$show_count)."</div>";	
	}
	if($sfsi_section8['sfsi_plus_rectpinit'] == 'yes')
	{
		if($show_count){$sfsiLikeWithpinit = "100px";}else{$sfsiLikeWithpinit = "auto";}
	 	$icons.="<div class='sf_pinit' style='display: inline-block;text-align:left;vertical-align: top;width: ".$sfsiLikeWithpinit.";'>".sfsi_plus_pinitpinterest($permalink,$show_count)."</div>";
	}
	if($sfsi_section8['sfsi_plus_rectlinkedin'] == 'yes')
	{
		if($show_count){$sfsiLikeWithlinkedin = "100px";}else{$sfsiLikeWithlinkedin = "auto";}
		$icons.="<div class='sf_linkedin' style='display: inline-block;vertical-align: middle;text-align:left;width: ".$sfsiLikeWithlinkedin."'>".sfsi_LinkedInShare($permalink,$show_count)."</div>";
	}
	if($sfsi_section8['sfsi_plus_rectreddit'] == 'yes')
	{
		if($show_count){$sfsiLikeWithreddit = "auto";}else{$sfsiLikeWithreddit = "auto";}
		$icons.="<div class='sf_reddit' style='display: inline-block;vertical-align: middle;text-align:left;width: ".$sfsiLikeWithreddit."'>".sfsi_redditShareButton($permalink)."</div>";
	}
	if($sfsi_section8['sfsi_plus_rectshr'] == 'yes')
	{
		$icons.="<div class='sf_addthis'  style='display: inline-block;vertical-align: middle;width: auto;margin-top: 6px;'>".sfsi_plus_Addthis($show_count, $permalink, $title)."</div>";
	}
	$icons.="</div>";
    
	if(!is_feed() && !is_home())
	{
		$content =   $content .$icons;
	}
	// var_dump($content);
	return $content;
}

function sfsi_premium_social_responsive_buttons($content,$option8,$server_side=false)
{
	
	if( (isset($option8["sfsi_plus_show_item_onposts"])&& $option8["sfsi_plus_show_item_onposts"]=="yes" && isset($option8["sfsi_plus_display_button_type"]) && $option8["sfsi_plus_display_button_type"]=="responsive_button") || $server_side ):
    $option2 = unserialize(get_option('sfsi_premium_section2_options',false));
    $option4 = unserialize(get_option('sfsi_premium_section4_options',false));
	$icons= "";
	$sfsi_premium_responsive_icons = (isset($option8["sfsi_plus_responsive_icons"])?$option8["sfsi_plus_responsive_icons"]:null);
	
	if(is_null($sfsi_premium_responsive_icons) ){
		return ""; // dont return anything if options not set;
	}
	$icon_width_type = $sfsi_premium_responsive_icons["settings"]["icon_width_type"];
	if($option4['sfsi_plus_display_counts']=='yes' &&isset($sfsi_premium_responsive_icons["settings"]['show_count'])&&$sfsi_premium_responsive_icons["settings"]['show_count']=="yes"):
       $counter_class="sfsi_premium_responsive_with_counter_icons";
       $couter_display="inline-block";
   	else:
       $counter_class="sfsi_premium_responsive_without_counter_icons";
       $couter_display="none";
    endif;

	$icons .= "<div class='sfsi_premium_responsive_icons' style='display:inline-block; ".($icon_width_type=="Fully Responsive"?"width:100%;display:flex; ":'width:100%')."' data-icon-width-type='".$icon_width_type."' data-icon-width-size='".$sfsi_premium_responsive_icons["settings"]['icon_width_size']."' data-edge-type='".$sfsi_premium_responsive_icons["settings"]['edge_type']."' data-edge-radius='".$sfsi_premium_responsive_icons["settings"]['edge_radius']."'  >";
	$sfsi_premium_anchor_div_style="";
	if($sfsi_premium_responsive_icons["settings"]["edge_type"]==="Round"){
		$sfsi_premium_anchor_div_style.=" border-radius:";
		if($sfsi_premium_responsive_icons["settings"]["edge_radius"]!==""){
			$sfsi_premium_anchor_div_style.=$sfsi_premium_responsive_icons["settings"]["edge_radius"].'px; ';
		}else{
			$sfsi_premium_anchor_div_style.='0px; ';
		}
	}
	
	ob_start();?>
			<div class="sfsi_premium_responsive_icons_count sfsi_premium_<?php echo ($icon_width_type=="Fully responsive"?'responsive':'fixed'); ?>_count_container sfsi_premium_<?php echo strtolower($sfsi_premium_responsive_icons['settings']['icon_size']); ?>_button"  style='display:<?php echo $couter_display; ?>;text-align:center; background-color:<?php echo $sfsi_premium_responsive_icons['settings']['counter_bg_color'] ;?>;color:<?php echo $sfsi_premium_responsive_icons['settings']['counter_color'] ;?>; <?php echo $sfsi_premium_anchor_div_style; ?>;' >
				<h3 style="color:<?php echo $sfsi_premium_responsive_icons['settings']['counter_color'] ;?>; " ><?php echo sfsi_premium_total_count($sfsi_premium_responsive_icons['default_icons']); ?></h3>
				<h6 style="color:<?php echo $sfsi_premium_responsive_icons['settings']['counter_color'] ;?>;" ><?php echo $sfsi_premium_responsive_icons['settings']["share_count_text"]; ?></h6>
			</div>
	<?php
		$icons.= ob_get_contents();
		ob_end_clean();
	$icons .= "\t<div class='sfsi_premium_icons_container ".$counter_class." sfsi_premium_".strtolower($sfsi_premium_responsive_icons['settings']['icon_size'])."_button_container sfsi_premium_icons_container_box_".($icon_width_type!=="Fixed icon width"?"fully":'fixed')."_container ' style='".($icon_width_type!=="Fixed icon width"?"width:100%;display:flex; ":'width:auto')."; text-align:center;' >";
	$socialObj = new sfsi_plus_SocialHelper();
	//styles
	$sfsi_premium_anchor_style="";
	if($sfsi_premium_responsive_icons["settings"]["text_align"]=="Centered"){
		$sfsi_premium_anchor_style.='text-align:center;';
	}
	if($sfsi_premium_responsive_icons["settings"]["margin"]!==""){
		$sfsi_premium_anchor_style.='margin-left:'.$sfsi_premium_responsive_icons["settings"]["margin"]."px; ";
		// $sfsi_premium_anchor_style.='margin-bottom:'.$sfsi_premium_responsive_icons["settings"]["margin"]."px; ";
	}
	//styles
	 
	if($sfsi_premium_responsive_icons['settings']['icon_width_type']==="Fixed icon width") 
	{
		$sfsi_premium_anchor_div_style.='width:'.$sfsi_premium_responsive_icons['settings']['icon_width_size'].'px;';
	}
	else{
		$sfsi_premium_anchor_style.=" flex-basis:100%;";
		$sfsi_premium_anchor_div_style.=" width:100%;";
	}
	// var_dump($sfsi_premium_anchor_style,$sfsi_premium_anchor_div_style);
	foreach($sfsi_premium_responsive_icons['default_icons'] as $icon=>$icon_config){
		// var_dump($icon_config);
		$current_url =  $socialObj->sfsi_get_custom_share_link(strtolower($icon));
		switch($icon){
			case "facebook":$share_url="https://www.facebook.com/sharer/sharer.php?u=".trailingslashit($current_url); break;
           	case "Twitter":$twitter_text = $socialObj->sfsi_get_custom_tweet_text();$share_url = "https://twitter.com/intent/tweet?text=".$twitter_text."&url=";break;
			case "Follow":
				if(isset($option2['sfsi_plus_email_icons_functions']) && $option2['sfsi_plus_email_icons_functions'] == 'sf')
				{
					$share_url = (isset($option2['sfsi_plus_email_url']))
						? $option2['sfsi_plus_email_url']
						: 'javascript:';	
				}
				elseif(isset($option2['sfsi_plus_email_icons_functions']) && $option2['sfsi_plus_email_icons_functions'] == 'contact')
				{
					$share_url = (isset($option2['sfsi_plus_email_icons_contact']) && !empty($option2['sfsi_plus_email_icons_contact']))
						? "mailto:".$option2['sfsi_plus_email_icons_contact']
						: 'javascript:';	
				}
				elseif(isset($option2['sfsi_plus_email_icons_functions']) && $option2['sfsi_plus_email_icons_functions'] == 'page')
				{
					$share_url = (isset($option2['sfsi_plus_email_icons_pageurl']) && !empty($option2['sfsi_plus_email_icons_pageurl']))
						? $option2['sfsi_plus_email_icons_pageurl']
						: 'javascript:';	
				}
				elseif(isset($option2['sfsi_plus_email_icons_functions']) && $option2['sfsi_plus_email_icons_functions'] == 'share_email')
				{
					$subject = stripslashes($option2['sfsi_plus_email_icons_subject_line']);
					$subject = str_replace('${title}', $socialObj->sfsi_get_the_title(), $subject);
					$subject = str_replace('"', '', str_replace("'", '', $subject));
					$subject = html_entity_decode(strip_tags($subject), ENT_QUOTES,'UTF-8');
					$subject = str_replace("%26%238230%3B", "...", $subject);				
					$subject = rawurlencode($subject);

					$body = stripslashes($option2['sfsi_plus_email_icons_email_content']);
					$body = str_replace('${title}', $socialObj->sfsi_get_the_title(), $body);	
					$body = str_replace('${link}',  trailingslashit($socialObj->sfsi_get_custom_share_link('email')), $body);
					$body = str_replace('"', '', str_replace("'", '', $body));
					$body = html_entity_decode(strip_tags($body), ENT_QUOTES,'UTF-8');
					$body = str_replace("%26%238230%3B", "...", $body);
					$body = rawurlencode($body);				
					$share_url = "mailto:?subject=$subject&body=$body";
				}
				else
				{
					$share_url = (isset($option2['sfsi_plus_email_url']))
						? $option2['sfsi_plus_email_url']
						: 'javascript:';	
				}
				break;
            case "pinterest":$share_url = "http://pinterest.com/pin/create/link/?url=".trailingslashit($current_url);break;
            case "Linkedin":$share_url = "http://www.linkedin.com/shareArticle?mini=true&url=".$current_url;break;
            case "Whatsapp":$share_url = wp_is_mobile()?'https://api.whatsapp.com/send?text='.$current_url:'https://web.whatsapp.com/send?text='.$current_url;break;
            case "vk":$share_url = "http://vk.com/share.php?url=".trailingslashit($current_url);break;
            case "Odnoklassniki":$share_url = "https://connect.ok.ru/offer?url=".trailingslashit($current_url);break;
            case "Telegram":$share_url = "https://telegram.me/share/url?url=".trailingslashit($current_url);break;
            case "Weibo":$share_url = "http://service.weibo.com/share/share.php?url=".trailingslashit($current_url);break;
            case "QQ2":$share_url = "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=".trailingslashit($current_url);break;
            case "xing":$share_url = "https://www.xing.com/app/user?op=share&url=".trailingslashit($current_url);break;
		}
		$icons.="\t\t"."<a ".sfsi_plus_checkNewWindow()." href='".($icon_config['url']==""?$share_url:$icon_config['url'])."' style='".($icon_config['active']=='yes'?($sfsi_premium_responsive_icons['settings']['icon_width_type']==="Fixed icon width"?'display:inline-flex':'display:block'):'display:none').";".$sfsi_premium_anchor_style."' class=".($sfsi_premium_responsive_icons['settings']['icon_width_type']==="Fixed icon width"?'sfsi_premium_responsive_fixed_width':'sfsi_premium_responsive_fluid')." >"."\n";
		$icons.="\t\t\t<div class='sfsi_premium_responsive_icon_item_container sfsi_premium_responsive_icon_".strtolower($icon)."_container sfsi_premium_".strtolower($sfsi_premium_responsive_icons['settings']['icon_size'])."_button ".($sfsi_premium_responsive_icons['settings']['style']=="Gradient"?'sfsi_premium_responsive_icon_gradient':'').( " sfsi_premium_".(strtolower($sfsi_premium_responsive_icons['settings']['text_align'])=="centered"?'centered':'left-align')."_icon")."' style='".$sfsi_premium_anchor_div_style." ' >"."\n";
		$icons.="\t\t\t\t<img style='max-height: 25px;display:unset;margin:0' src='".SFSI_PLUS_PLUGURL."images/responsive-icon/".$icon.('Follow'===$icon?'.png':'.svg')."'>"."\n";
		$icons.="\t\t\t\t<span style='color:#fff' >".($icon_config["text"])."</span>"."\n";
		$icons.="\t\t\t</div>"."\n";
		$icons.="\t\t</a>"."\n\n";
	}
	$sfsi_premium_responsive_icons_custom_icons=array();
	if(!isset($sfsi_premium_responsive_icons['custom_icons']) || !empty($sfsi_premium_responsive_icons['custom_icons'])){
		$sfsi_premium_responsive_icons_custom_icons=$sfsi_premium_responsive_icons['custom_icons'];
	}else{
		$count=5;
		for($i=0;$i<$count;$i++){
			array_push($sfsi_premium_responsive_icons_custom_icons,array(
				"added"=>"no",
				"active"=>"no",
				"text"=>"Share",
				"bg-color"=>"#729fcf",
				"url"=>"",
				"icon"=>''
			));
		}
	}
	foreach($sfsi_premium_responsive_icons_custom_icons as $icon=>$icon_config){
		// var_dump($icon_config);
		$current_url =  $socialObj->sfsi_get_custom_share_link(strtolower($icon));
		$icons.="\t\t"."<a ".sfsi_plus_checkNewWindow()." href='".($icon_config['url']==""?"":$icon_config['url'])."' style='".($icon_config['active']=='yes'?'display:inline-flex':'display:none').";".$sfsi_premium_anchor_style."' class=".($sfsi_premium_responsive_icons['settings']['icon_width_type']==="Fixed icon width"?'sfsi_premium_responsive_fixed_width':'sfsi_premium_responsive_fluid')."  >"."\n";
		$icons.="\t\t\t<div class='sfsi_premium_responsive_icon_item_container sfsi_premium_responsive_custom_icon sfsi_premium_responsive_icon_".strtolower($icon)."_container sfsi_premium_".strtolower($sfsi_premium_responsive_icons['settings']['icon_size'])."_button ".( "sfsi_premium_".(strtolower($sfsi_premium_responsive_icons['settings']['text_align'])=="centered"?'centered':'left-align')."_icon")." ".($sfsi_premium_responsive_icons['settings']['style']=="Gradient"?'sfsi_premium_responsive_icon_gradient':'')."' style='".$sfsi_premium_anchor_div_style." background-color:".(isset($icon_config['bg-color'])?$icon_config['bg-color']:'#73d17c')."' >"."\n";
		$icons.="\t\t\t\t<img style='max-height: 25px' src='".(isset($icon_config['icon'])?$icon_config['icon']:'#')."'>"."\n";
		$icons.="\t\t\t\t<span style='color:#fff' >".($icon_config["text"])."</span>"."\n";
		$icons.="\t\t\t</div>"."\n";
		$icons.="\t\t</a>"."\n\n";
	}
	$icons.="</div></div><!--end responsive_icons-->";
	return $icons;
	endif;
}

function sfsi_premium_total_count($icons){
	// ini_set('max_execution_time', 300);
	// $count = 0;
    $socialObj = new sfsi_plus_SocialHelper();
	// foreach($icons as $icon_name=>$icon){
	// 	if($icon['active']=="yes"){
	// 		switch(strtolower($icon_name) ){
	// 			case 'facebook': $count+=(isset($icon_counts['fb_count'])?$icon_counts['fb_count']:0);break;
	// 			case 'twitter': $count+=(isset($icon_counts['twitter_count'])?$icon_counts['twitter_count']:0);break;
	// 			case 'follow': $count+=(isset($icon_counts['email_count'])?$icon_counts['email_count']:0);break;
	// 			case 'pinterest': $count+=(isset($icon_counts['pin_count'])?$icon_counts['pin_count']:0);break;
	// 			case 'linkedin': $count+=(isset($icon_counts['linkedIn_count'])?$icon_counts['linkedIn_count']:0);break;
	// 			case 'Whatsapp': $count+=(isset($icon_counts['whatsapp_count'])?$icon_counts['whatsapp_count']:0);break;
	// 			case 'vk': $count+=(isset($icon_counts['vk_count'])?$icon_counts['vk_count']:0);break;
	// 		}
	// 	}
	// }
	$count = get_option('sfsi_premium_icon_counts',0);
	return $socialObj->format_num($count);
}


/*subscribe like*/
function sfsi_plus_Subscribelike($permalink, $show_count)
{
	global $socialObj;
	$socialObj = new sfsi_plus_SocialHelper();
	
	$sfsi_premium_section2_options=  unserialize(get_option('sfsi_premium_section2_options',false));
	$option4 = unserialize(get_option('sfsi_premium_section4_options',false));
	$sfsi_premium_section8_options=  unserialize(get_option('sfsi_premium_section8_options',false));
	$option5 =  unserialize(get_option('sfsi_premium_section5_options',false));
	
	$post_icons     = $option5['sfsi_plus_follow_icons_language'];
	$visit_icon1    = SFSI_PLUS_DOCROOT.'/images/visit_icons/Follow/icon_'.$post_icons.'.png';
	$visit_iconsUrl = SFSI_PLUS_PLUGURL."images/";
		   
	if(file_exists($visit_icon1))
	{
		$visit_icon = $visit_iconsUrl."visit_icons/Follow/icon_".$post_icons.".png";
	}
	else
	{
		$visit_icon = $visit_iconsUrl."follow_subscribe.png";
	}
	
	$url = (isset($sfsi_premium_section2_options['sfsi_plus_email_url'])) ? $sfsi_premium_section2_options['sfsi_plus_email_url'] : 'javascript:void(0);';
	
	if($option4['sfsi_plus_email_countsFrom']=="manual")
	{    
		$counts=$socialObj->format_num($option4['sfsi_plus_email_manualCounts']);
	}
	else
	{
		$counts= $socialObj->SFSI_getFeedSubscriber(sanitize_text_field(get_option('sfsi_premium_feed_id',false)));           
	}
	 
	if($sfsi_premium_section8_options['sfsi_plus_icons_DisplayCounts']=="yes")
	{
	 	$icon = '<a href="'.$url.'" target="_blank"><img data-pin-nopin="true" src="'.$visit_icon.'" alt="onpost_follow" /></a>
		<span class="bot_no">'.$counts.'</span>';
	}
	else
	{
	 	$icon = '<a href="'.$url.'" target="_blank"><img data-pin-nopin="true" src="'.$visit_icon.'" alt="onpost_follow" /></a>';
	}
	return $icon;
}
/*subscribe like*/

/*twitter like*/
function sfsi_plus_twitterlike($permalink, $show_count)
{
	global $socialObj;
	$socialObj = new sfsi_plus_SocialHelper();
	$twitter_text = '';
	$sfsi_premium_section5_options = unserialize(get_option('sfsi_premium_section5_options',false));
	$icons_language = $sfsi_premium_section5_options['sfsi_plus_icons_language'];
	
	$postid =  $socialObj->sfsi_get_the_ID();

	if(!empty($postid))
	{
		$twitter_text = $socialObj->sfsi_get_custom_tweet_title();
	}
	return $socialObj->sfsi_twitterSharewithcount($permalink,$twitter_text, $show_count, $icons_language);
}
/*twitter like*/


/* create fb like button */
function sfsi_plus_FBlike($permalink,$show_count)
{
    $send = 'false';
	$width = 180;
	$option8=  unserialize(get_option('sfsi_premium_section8_options',false));
	$fb_like_html = '';

	if($option8['sfsi_plus_rectfbshare'] == 'yes' && $option8['sfsi_plus_rectfb'] == 'yes')
	{
		$fb_like_html .='<div class="fb-like" data-href="'.$permalink.'" data-action="like" data-share="true"';
	}
	else if($option8['sfsi_plus_rectfb'] == 'no' && $option8['sfsi_plus_rectfbshare'] == 'yes')
	{
		$fb_like_html .= '<div class="fb-share-button" data-href="'.$permalink.'" ';
	}
	else
	{
		$fb_like_html .= '<div class="fb-like" data-href="'.$permalink.'" data-action="like" data-share="false"';
	}
	if($show_count==1)
	{ 
		$fb_like_html .= ' data-layout="button_count"';
	}
	else
	{
		$fb_like_html .= ' data-layout="button"';
	}
	$fb_like_html .= ' ></div>';
	return $fb_like_html;
}


/* create pinit button */
function sfsi_plus_pinitpinterest($permalink,$show_count)
{
	//******************* Get custom meta data set in admin STARTS **************************************//
	$socialObj    = new sfsi_plus_SocialHelper();

	// $pin_it_html    = '<a data-pin-do="buttonPin" data-pin-save="true" href="https://www.pinterest.com/pin/create/button/?url='.$url.'&media='.$pinterest_img.'&description='.$pinterest_desc.'"></a>';

	$pinit_html = '<a href="https://www.pinterest.com/pin/create/button/?url='.$permalink.'&media='.$socialObj->sfsi_pinit_image().'&description='.$socialObj->sfsi_pinit_description().'" data-pin-do="buttonPin" data-pin-save="true"';
	if($show_count)
	{
		$pinit_html .= ' data-pin-count="beside"';
	}
	else
	{
		$pinit_html .= ' data-pin-count="none"';
	}
	$pinit_html .= '></a>';
	
	return $pinit_html;
}

/* create add this  button */
function sfsi_plus_Addthis($show_count, $permalink, $post_title)
{
   	$atiocn=' <script type="text/javascript">
		var addthis_config = {
			url: "'.$permalink.'",
			title: "'.$post_title.'"
		}
	</script>';

	if($show_count==1)
	{
		$atiocn.=' <div class="addthis_toolbox" addthis:url="'.$permalink.'" addthis:title="'.$post_title.'">
			<a class="addthis_counter addthis_pill_style share_showhide"></a>
		</div>';
		return $atiocn;
	}
	else
	{
		$atiocn.='<div class="addthis_toolbox addthis_default_style addthis_20x20_style" addthis:url="'.$permalink.'" addthis:title="'.$post_title.'"><a class="addthis_button_compact " href="#">  <img data-pin-nopin="true" src="'.SFSI_PLUS_PLUGURL.'images/sharebtn.png"  border="0" alt="Share" /></a></div>';
		return $atiocn;
    }
}

function sfsi_plus_Addthis_blogpost($show_count, $permalink, $post_title)
{ 
	$atiocn=' <script type="text/javascript">
		var addthis_config = {
			 url: "'.$permalink.'",
			 title: "'.$post_title.'"
		}
	</script>';
	
	if($show_count==1)
	{
	   $atiocn.=' <div class="addthis_toolbox" addthis:url="'.$permalink.'" addthis:title="'.$post_title.'">
              <a class="addthis_counter addthis_pill_style share_showhide"></a>
	   </div>';
	    return $atiocn;
	}
	else
	{
		$atiocn.='<div class="addthis_toolbox addthis_default_style addthis_20x20_style" addthis:url="'.$permalink.'" addthis:title="'.$post_title.'"><a class="addthis_button_compact " href="#">  <img data-pin-nopin="true" src="'.SFSI_PLUS_PLUGURL.'images/sharebtn.png"  border="0" alt="Share" /></a></div>';
		return $atiocn; 
    }
}

/* create linkedIn  share button */
function sfsi_LinkedInShare($url='', $count='')
{
	$url= (isset($url))? $url :  home_url();

	if($count == 1)
	{
		return  $ifollow='<script type="IN/Share" data-url="'.$url.'" data-counter="right"></script>';
	}
	else
	{
		return  $ifollow='<script type="IN/Share" data-url="'.$url.'" ></script>';
	}
}

/* create linkedIn  share button */
function sfsi_redditShareButton($url='')
{
	$url = (isset($url))? $url :  home_url();
	$url = "//www.reddit.com/submit?url=$url";
	$onclick = "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=800');return false;";
	return  $ifollow='<a href="'.$url.'" onclick="'.$onclick.'"> <img data-pin-nopin="true" src="//www.redditstatic.com/spreddit7.gif" alt="submit to reddit" style="border:0" /> </a>';
}
?>
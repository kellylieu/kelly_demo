<?php

if (!defined('ABSPATH')) die('No direct access.');

/**
 *
 * Semaphore Lock Management
 *
 * Adapted originally from WP Social under the GPL - thanks to Alex King (https://github.com/crowdfavorite/wp-social)
 * 
 * Example of usage:
 * 
 * require_once('class-semaphore.php');
 * 
 * $semaphore = 'lock_name';
 * 
 * $semaphore = new Updraft_Semaphore($semaphore);
 * 
 * $semaphore_log_message = 'Requesting semaphore lock ('.$semaphore.')';
 * 
 * error_log($semaphore_log_message);
 *
 * if (!$semaphore->lock()) {
 * 
 *	error_log('Failed to gain semaphore lock ('.$semaphore.') - another backup of this type is apparently already active - aborting (if this is wrong - i.e. if the other backup crashed without removing the lock, then another can be started after 3 minutes)');
 * 	return;
 * 	
 * }
 * 
 */

class Updraft_Semaphore {

	/**
	 * @var bool
	 */
	protected $lock_broke = false;

	public $lock_name;
	
	protected $logger;
	
	protected $prefix;
	
	protected $release_after;
	
	/**
	 * Constructor for the semaphore object.
	 *
	 * @param String $lock_name		 - a name for the lock
	 * @param Callable|Null $logger	 - a callable to be called with a log line as the parameter
	 * @param Integer $release_after - how many minutes to release the lock after
	 * @param String $prefix		 - a prefix for the lock (not usually needed)
	 */
	public function __construct($lock_name, $logger = null, $release_after = 3, $prefix = 'updraft_') {
		$this->lock_name = $lock_name;
		$this->logger = $logger;
		$this->prefix = $prefix;
		$this->release_after = $release_after;
		$this->init();
	}
	
	/**
	 * Attempts to start the lock. If the rename works, the lock is started.
	 *
	 * @return bool
	 */
	public function lock() {
		$wpdb = $GLOBALS['wpdb'];

		// Attempt to set the lock
		$affected = $wpdb->query("
			UPDATE $wpdb->options
			   SET option_name = '".$this->prefix."locked_".$this->lock_name."'
			 WHERE option_name = '".$this->prefix."unlocked_".$this->lock_name."'
		");

		if ($affected == '0' and !$this->stuck_check()) {
			$this->log('Semaphore lock ('.$this->lock_name.', '.$wpdb->options.') failed (line '.__LINE__.')');
			return false;
		}

		// Check to see if all processes are complete
		$affected = $wpdb->query("
			UPDATE $wpdb->options
			   SET option_value = CAST(option_value AS UNSIGNED) + 1
			 WHERE option_name = '".$this->prefix."semaphore_".$this->lock_name."'
			   AND option_value = '0'
		");
		
		if ($affected != '1') {
			if (!$this->stuck_check()) {
				$this->log('Semaphore lock ('.$this->lock_name.', '.$wpdb->options.') failed (line '.__LINE__.')');
				return false;
			}

			// Reset the semaphore to 1
			$wpdb->query("
				UPDATE $wpdb->options
				   SET option_value = '1'
				 WHERE option_name = '".$this->prefix."semaphore_".$this->lock_name."'
			");

			$this->log('Semaphore ('.$this->lock_name.', '.$wpdb->options.') reset to 1');
		}

		// Set the lock time
		$wpdb->query($wpdb->prepare("
			UPDATE $wpdb->options
			   SET option_value = %s
			 WHERE option_name = '".$this->prefix."last_lock_time_".$this->lock_name."'
		", current_time('mysql', 1)));
		$this->log('Set semaphore last lock ('.$this->lock_name.') time to '.current_time('mysql', 1));

		$this->log('Semaphore lock ('.$this->lock_name.') complete');
		return true;
	}
	
	/**
	 * Log information
	 * 
	 * @param String $line - the line to log
	 */
	private function log($line) {
		if (is_callable($this->logger)) return call_user_func($this->logger, $line);
	}
	
	/**
	 * Initialisation function; called by the constructor
	 */
	private function init() {
		// Make sure the options for semaphores exist
		$wpdb = $GLOBALS['wpdb'];
		$results = $wpdb->get_results("
			SELECT option_id
				FROM $wpdb->options
				WHERE option_name IN ('".$this->prefix."locked_".$this->lock_name."', '".$this->prefix."unlocked_".$this->lock_name."', '".$this->prefix."last_lock_time_".$this->lock_name."', '".$this->prefix."semaphore_".$this->lock_name."')
		");

		if (!is_array($results) || count($results) < 3) {
		
			if (is_array($results) && count($results) > 0) {
				$this->log("Semaphore (".$this->lock_name.", ".$wpdb->options.") in an impossible/broken state - fixing (".count($results).")");
			} else {
				$this->log("Semaphore (".$this->lock_name.", ".$wpdb->options.") being initialised");
			}
			
			$wpdb->query("
				DELETE FROM $wpdb->options
				WHERE option_name IN ('".$this->prefix."locked_".$this->lock_name."', '".$this->prefix."unlocked_".$this->lock_name."', '".$this->prefix."last_lock_time_".$this->lock_name."', '".$this->prefix."semaphore_".$this->lock_name."')
			");
			
			$wpdb->query($wpdb->prepare("
				INSERT INTO $wpdb->options (option_name, option_value, autoload)
				VALUES
				('".$this->prefix."unlocked_".$this->lock_name."', '1', 'no'),
				('".$this->prefix."last_lock_time_".$this->lock_name."', '%s', 'no'),
				('".$this->prefix."semaphore_".$this->lock_name."', '0', 'no')
			", current_time('mysql', 1)));
		}
	}
	
	/**
	 * Increment the semaphore.
	 *
	 * @param  array  $filters
	 * @return Social_Semaphore
	 */
	public function increment(array $filters = array()) {
		$wpdb = $GLOBALS['wpdb'];

		if (count($filters)) {
			// Loop through all of the filters and increment the semaphore
			foreach ($filters as $priority) {
				for ($i = 0, $j = count($priority); $i < $j; ++$i) {
					$this->increment();
				}
			}
		}
		else {
			$wpdb->query("
				UPDATE $wpdb->options
				   SET option_value = CAST(option_value AS UNSIGNED) + 1
				 WHERE option_name = '".$this->prefix."semaphore_".$this->lock_name."'
			");
			$this->log('Incremented the semaphore ('.$this->lock_name.') by 1');
		}

		return $this;
	}

	/**
	 * Decrements the semaphore.
	 *
	 * @return void
	 */
	public function decrement() {
		$wpdb = $GLOBALS['wpdb'];

		$wpdb->query("
			UPDATE $wpdb->options
			   SET option_value = CAST(option_value AS UNSIGNED) - 1
			 WHERE option_name = '".$this->prefix."semaphore_".$this->lock_name."'
			   AND CAST(option_value AS UNSIGNED) > 0
		");
		$this->log('Decremented the semaphore ('.$this->lock_name.') by 1');
	}

	/**
	 * Unlocks the process.
	 *
	 * @return bool
	 */
	public function unlock() {
		$wpdb = $GLOBALS['wpdb'];

		// Decrement for the master process.
		$this->decrement();

		$result = $wpdb->query("
			UPDATE $wpdb->options
			   SET option_name = '".$this->prefix."unlocked_".$this->lock_name."'
			 WHERE option_name = '".$this->prefix."locked_".$this->lock_name."'
		");

		if ($result == '1') {
			$this->log('Semaphore ('.$this->lock_name.') unlocked');
			return true;
		}

		$this->log('Semaphore ('.$this->lock_name.', '.$wpdb->options.') still locked ('.$result.')');
		return false;
	}

	/**
	 * Attempts to jiggle the stuck lock loose.
	 *
	 * @return bool
	 */
	private function stuck_check() {
		$wpdb = $GLOBALS['wpdb'];

		// Check to see if we already broke the lock.
		if ($this->lock_broke) {
			return true;
		}

		$current_time = current_time('mysql', 1);
		$minutes_before = gmdate('Y-m-d H:i:s', time()-(defined('UPDRAFT_SEMAPHORE_LOCK_WAIT') ? UPDRAFT_SEMAPHORE_LOCK_WAIT : $this->release_after*60));

		$affected = $wpdb->query($wpdb->prepare("
			UPDATE $wpdb->options
			   SET option_value = %s
			 WHERE option_name = '".$this->prefix."last_lock_time_".$this->lock_name."'
			   AND option_value <= %s
		", $current_time, $minutes_before));

		if ('1' == $affected) {
			$this->log('Semaphore ('.$this->lock_name.', '.$wpdb->options.') was stuck, set lock time to '.$current_time);
			$this->lock_broke = true;
			return true;
		}

		return false;
	}

}


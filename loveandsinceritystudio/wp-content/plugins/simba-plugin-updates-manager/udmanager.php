<?php
/*
Plugin Name: Simba Plugins Manager
Plugin URI: https://www.simbahosting.co.uk/s3/shop/
Description: Management of plugin updates + licences
Author: David Anderson
Version: 1.9.27
License: GPLv2+ / MIT
Text Domain: simba-plugin-updates-manager
Author URI: https://david.dw-perspective.org.uk
*/

if (!defined('ABSPATH')) die('No direct access.');

define('UDMANAGER_VERSION', '1.9.27');
define('UDMANAGER_DIR', dirname(realpath(__FILE__)));
define('UDMANAGER_URL', plugins_url('', realpath(__FILE__)));
define('UDMANAGER_SLUG', basename(UDMANAGER_DIR));

require_once(UDMANAGER_DIR.'/options.php');
require_once(UDMANAGER_DIR.'/classes/updraftmanager.php');

if (!defined('UDMANAGER_DISABLEPREMIUM') || !UDMANAGER_DISABLEPREMIUM) @include_once(UDMANAGER_DIR.'/premium/load.php');

if (empty($updraftmanager_options) || !is_object($updraftmanager_options) || !is_a($updraftmanager_options, 'UpdraftManager_Options')) $updraftmanager_options = new UpdraftManager_Options;

$updraft_manager = new Updraft_Manager;

register_activation_hook(__FILE__, array($updraft_manager, 'activation'));
register_deactivation_hook(__FILE__, array($updraft_manager, 'deactivation'));
